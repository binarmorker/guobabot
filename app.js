const { ClientCredentialsAuthProvider } = require('@twurple/auth')
const { ApiClient } = require('@twurple/api')
const { ReverseProxyAdapter, EventSubListener } = require('@twurple/eventsub')
const { NgrokAdapter } = require('@twurple/eventsub-ngrok')
const { Client, Collection, Intents } = require('discord.js')
const { REST } = require('@discordjs/rest')
const { Routes } = require('discord-api-types/v9')
const flatfile = require('flatfile')
const pino = require('pino')
const fs = require('fs')
const http = require('http')
const { URL } = require('url')
require('dotenv').config()

const logger = pino({ level: process.env.LOG_LEVEL || 'info', transport: { target: 'pino-pretty', options: { colorize: true, translateTime: true } } })

// Twitch client
const authProvider = new ClientCredentialsAuthProvider(process.env.TWITCH_CLIENT_ID, process.env.TWITCH_CLIENT_SECRET)
const apiClient = new ApiClient({ authProvider, logger: { minLevel: process.env.LOG_LEVEL || 'info' } })
const adapter = process.env.NODE_ENV === 'production'
  ? new ReverseProxyAdapter({
      hostName: process.env.TWITCH_CLIENT_DOMAIN,
      port: process.env.TWITCH_CLIENT_PORT
    })
  : new NgrokAdapter()
const listener = new EventSubListener({ apiClient, adapter, secret: process.env.DISCORD_BOT_SECRET, logger: { minLevel: process.env.LOG_LEVEL || 'info' } })
logger.info('Twitch client initialized')

// Discord bot
const client = new Client({ intents: [Intents.FLAGS.GUILDS] })
client.login(process.env.DISCORD_BOT_TOKEN)
client.commands = new Collection()
const commands = []
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'))

for (const file of commandFiles) {
  const command = require(`./commands/${file}`)(client, apiClient)
  commands.push(command.data.toJSON())
  client.commands.set(command.data.name, command)
  logger.info(`Added Discord command /${command.data.name}`)
}

client.on('interactionCreate', async interaction => {
  if (!interaction?.isCommand()) return

  const command = client.commands.get(interaction.commandName)

  if (!command) return

  try {
    await command.execute(interaction)
  } catch (error) {
    logger.error(error)
    return interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true })
  }
})

client.on("guildCreate", guild => {
  logger.info(`New guild joined: ${guild.name} (id: ${guild.id}). This guild has ${guild.memberCount} members!`)
})

client.on("guildDelete", async guild => {
  logger.info(`I have been removed from: ${guild.name} (id: ${guild.id})`)
  await flatfile.db('data.json', async (err, data) => {
    if (err) throw err

    if (data && Object.keys(data).includes(guild.id)) {
      delete data[guild.id]
			await data.save(err => { if (err) throw err })
      logger.debug(`Removed ${guild.name} (id: ${guild.id}) from database`)
    }
  })
})

// HTTP server
const server = http.createServer((req, res) => {
  logger.info(req.url)
  logger.info(res)

  switch (new URL(req.url, process.env.HTTP_DOMAIN).pathname) {
    case '/':
      res.writeHead(200, { 'content-type': 'text/html' })
      const index = fs.createReadStream('index.html')
      index.pipe(res)
      break;
    case '/invite':
      res.writeHead(302, { 'Location': process.env.DISCORD_BOT_INVITE })
      res.end()
      break;
    case '/thumbnail.png':
      res.writeHead(200, { 'content-type': 'image/png' })
      const thumbnail = fs.createReadStream('thumbnail.png')
      thumbnail.pipe(res)
      break;
    default:
      res.writeHead(200, { 'content-type': 'text/html' })
      const notFound = fs.createReadStream('404.html')
      notFound.pipe(res)
      break;
  }
})

// Start
client.on('ready', async () => {
  await listener.listen()

  if (process.env.TWITCH_LISTENER_CLEANUP) {
    await apiClient.eventSub.deleteAllSubscriptions()
  }

  await listener.subscribeToStreamOnlineEvents(process.env.TWITCH_USER_ID, async e => {
    const stream = await e.getStream()
    client.commands.get('send-online-notification').send({
      name: e.broadcasterDisplayName,
      title: stream.title
    })
  })

  await listener.subscribeToStreamOfflineEvents(process.env.TWITCH_USER_ID, async e => {
    client.commands.get('clear-notifications').remove({
      name: e.broadcasterDisplayName
    })
  })

  server.listen(process.env.HTTP_PORT)
  
  const rest = new REST({ version: '9' }).setToken(process.env.DISCORD_BOT_TOKEN)
  await rest.put(Routes.applicationCommands(client.user.id), { body: commands })

  logger.info('GuobaBot is ready!')
})
