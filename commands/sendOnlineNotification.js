const { SlashCommandBuilder } = require('@discordjs/builders')
const { MessageEmbed } = require('discord.js')
const flatfile = require('flatfile')
const pino = require('pino')

const logger = pino({ level: process.env.LOG_LEVEL || 'info', transport: { target: 'pino-pretty', options: { colorize: true } } })

module.exports = (client, apiClient) => ({
	data: new SlashCommandBuilder()
		.setName('send-online-notification')
		.setDescription('Sends a Twitch online notification.'),
	async execute(interaction) {
		const stream = await apiClient.streams.getStreamByUserId(process.env.TWITCH_USER_ID)
		
		if (stream) {
			this.send({
				name: stream.userDisplayName,
				title: stream.title
			})
			await interaction.reply({ content: 'Notification sent!', ephemeral: true })
		} else {
			await interaction.reply({ content: 'Streamer is offline', ephemeral: true })
		}
	},
	async send(e) {
    logger.info(`${e.name} is live!`)

    await flatfile.db('data.json', async (err, data) => {
      if (err) throw err

      for (guildId in data) {
        const guild = client.guilds.cache.get(guildId)
        const channel = guild.channels.cache.get(data[guildId].onlineChannel)
        const embed = new MessageEmbed()
          .setColor('#0099ff')
          .setTitle(`${e.name} is live!`)
          .setDescription(e.title)
          .setURL(process.env.TWITCH_CHANNEL_URL)
          .setAuthor('GuobaBot')
          .setThumbnail(process.env.TWITCH_CHANNEL_THUMBNAIL)
        await channel.send({ embeds: [embed] })
        logger.info(`Sent message to channel #${channel.name} in server ${guild.name}.`)
      }
    })
  }
})
