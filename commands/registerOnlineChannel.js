const { SlashCommandBuilder } = require('@discordjs/builders')
const flatfile = require('flatfile')
const pino = require('pino')

const logger = pino({ level: process.env.LOG_LEVEL || 'info', transport: { target: 'pino-pretty', options: { colorize: true } } })

module.exports = () => ({
	data: new SlashCommandBuilder()
		.setName('register-online-channel')
		.setDescription('Registers the channel to use for Twitch online notifications.')
		.addChannelOption(channel => channel
			.setName('channel')
			.setDescription('Channel you want to send the Twitch online notifications in')
			.setRequired(true)
		),
	async execute(interaction) {
		await flatfile.db('data.json', async (err, data) => {
			if (err) throw err

			if (!data || !Object.keys(data).includes(interaction.guildId)) {
				data[interaction.guildId] = {
					onlineChannel: interaction.options.getChannel('channel').id
				}
			} else {
				data[interaction.guildId].onlineChannel = interaction.options.getChannel('channel').id
			}

			await data.save(err => { if (err) throw err })
			logger.info(`Saved online channel ${data[interaction.guildId].onlineChannel} for guild ${interaction.guildId}`)
			await interaction.reply({ content: 'Set channel successfully!', ephemeral: true })
		})
	}
})
