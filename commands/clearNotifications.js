const { SlashCommandBuilder } = require('@discordjs/builders')
const flatfile = require('flatfile')
const pino = require('pino')

const logger = pino({ level: process.env.LOG_LEVEL || 'info', transport: { target: 'pino-pretty', options: { colorize: true } } })

module.exports = client => ({
	data: new SlashCommandBuilder()
		.setName('clear-notifications')
		.setDescription('Clears all Twitch online notifications.'),
	async execute(interaction) {
    await flatfile.db('data.json', async (err, data) => {
      if (err) throw err
      
      for (guildId in data) {
        const guild = client.guilds.cache.get(guildId)
        const channel = guild.channels.cache.get(data[guildId].onlineChannel)
        const messages = await channel.messages.fetch({ limit: 100 })
        const botMessages = messages.filter(m => m.author.id === client.user.id)
        await channel.bulkDelete(botMessages)
        logger.info(`Deleted ${botMessages.size} messages from channel #${channel.name} in server ${guild.name}.`)

        if (interaction) {
          await interaction.reply({ content: 'Messages deleted', ephemeral: true })
        }
      }
    })
	},
	async remove(e) {
    logger.info(`${e.name} is offline.`)
    await this.execute()
	}
})
