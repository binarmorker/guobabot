const { SlashCommandBuilder } = require('@discordjs/builders')

module.exports = client => ({
	data: new SlashCommandBuilder()
		.setName('say')
		.setDescription('Make GuobaBot say your message.')
		.addStringOption(option => option
			.setName('message')
			.setDescription('The message you want GuobaBot to say')
			.setRequired(true)
		),
	async execute(interaction) {
		await client.channels.cache.get(interaction.channelId).send(interaction.options.getString('message'))
		await interaction.reply({ content: 'Message sent!', ephemeral: true })
	}
})
