module.exports = {
  apps: [
    {
      name: 'GuobaBot',
      script: './app.js',
      env: {
        'NODE_ENV': 'production'
      }
    }
  ]
}
